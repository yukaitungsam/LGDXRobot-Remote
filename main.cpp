#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQmlContext>
#include <QtWebView>

#include "System.h"
#include "SystemData.h"

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QtWebView::initialize();
    QGuiApplication app(argc, argv);

    /*
     * Default font
     */
    QFont font;
    font.setFamily("Source Han Sans HK");
    app.setFont(font);

    QQuickStyle::setStyle("Material");

    // QML Enums
    qRegisterMetaType<TCPConnectState>("TCPConnectState");
    qmlRegisterUncreatableType<TCPConnectStateClass>("TCPConnectStateClass", 1, 0, "TCPConnectState", "Not creatable as it is an enum type");
    qRegisterMetaType<RobotState>("RobotState");
    qmlRegisterUncreatableType<RobotStateClass>("RobotStateClass", 1, 0, "RobotState", "Not creatable as it is an enum type");

    QQmlApplicationEngine engine;
    System *system = System::getInstance();
    SystemData *data = SystemData::getInstance();
    QQmlContext* context = engine.rootContext();
    context->setContextProperty("system", system);
    context->setContextProperty("systemData", data);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
