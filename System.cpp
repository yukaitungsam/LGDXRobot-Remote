#include "System.h"
System *System::instance;

System::System(QObject *parent) : QObject(parent)
{
    data = SystemData::getInstance();
    // Initalize TCP
    tcpInServer = std::make_unique<QTcpServer>(this);
    if(tcpInServer->listen(QHostAddress::Any, kMyPort))
    {
        qDebug() << "TCP server is starting, the TCP port is:" << kMyPort;
        connect(tcpInServer.get(), &QTcpServer::newConnection, this, &System::tcpInNewConnection);
    }
    else
    {
        qDebug() << "TCP server is unable to start";
        exit(0);
    }
    tcpOutClient = std::make_unique<QTcpSocket>(this);
    QObject::connect(
        tcpOutClient.get(), &QTcpSocket::connected,
        [=]() {
        qDebug() << "TCP Connected to" << data->getRobotIP() << ":" << kRemotePort;
        data->setTcpConnectState(TCPConnectState::Connected);
        remoteHello();
    });
    QObject::connect(
        tcpOutClient.get(), &QTcpSocket::errorOccurred,
        [=]() {
        qDebug() << "TCP Error:" << tcpOutClient->errorString();
        data->setTcpConnectState(TCPConnectState::Error);
        data->setTcpConnectError(tcpOutClient->errorString());
    });
    QObject::connect(
        tcpOutClient.get(), &QTcpSocket::disconnected,
        [=]() {
        qDebug() << "TCP Disconnected";
        data->setTcpConnectState(TCPConnectState::Disconnected);
        data->clearSensorData();
    });
}

System *System::getInstance()
{
    if(instance == nullptr)
    {
        instance = new System();
    }
    return instance;
}

void System::remoteHello()
{
    tcpSend("hello");
}

void System::remoteAutoCollection()
{
    tcpSend("collection");
}

void System::remoteAutoExplore()
{
    tcpSend("autoexplore");
}

void System::remoteManual()
{
    tcpSend("manualall");
}

void System::remoteManualVelocity(float x, float y, float w)
{
    QString str = "manualik," + QString::number(x) + "," + QString::number(y) + "," + QString::number(w);
    tcpSend(str);
}

void System::remoteManualServo(int node, int angle)
{
    QString str = "manualservo," + QString::number(node) + "," + QString::number(angle);
    tcpSend(str);
}

void System::remoteIdle()
{
    tcpSend("stopall");
}

void System::tcpConnect(QString ip)
{
    data->setRobotIP(ip);
    //tcpOutClient->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
    tcpOutClient->connectToHost(ip, kRemotePort);
    data->setTcpConnectState(TCPConnectState::Connecting);
}

void System::tcpSend(QString str)
{
    if(tcpOutClient->state() != QTcpSocket::ConnectedState)
    {
        tcpConnect(data->getRobotIP());
        return;
    }
    else
    {
        tcpOutClient->write(str.toLocal8Bit());
    }
}

void System::tcpInNewConnection()
{
    tcpInClient = tcpInServer->nextPendingConnection();
    qDebug() << "A new TCP connection form %s" << tcpInClient->peerAddress();
    connect(tcpInClient, &QTcpSocket::readyRead, this, &System::tcpInReceive);
    connect(tcpInClient, &QTcpSocket::disconnected, tcpInClient, &QTcpSocket::deleteLater);
}

void System::tcpInReceive()
{
    QByteArray datas = tcpInClient->readAll();
    QString dataStr = QString::fromLocal8Bit(datas);
    //qDebug() << dataStr;
    QStringList dataList = dataStr.split("\n");
    for(int i = 0; i < dataList.size(); i++)
    {
        QStringList cmdList = dataList.at(i).split(",");
        if(cmdList.at(0).contains("hw"))
        {
            if(cmdList.size() != 3)
            {
                qDebug() << "hw cmd size error";
                continue;
            }
            if(cmdList.at(1).contains("robot"))
            {
                data->setRobotState(cmdList.at(2).toInt());
            }
            else if(cmdList.at(1).contains("d400"))
            {
                data->setD400(cmdList.at(2).toInt());
            }
            else if(cmdList.at(1).contains("t265"))
            {
                data->setT265(cmdList.at(2).toInt());
            }
            else if(cmdList.at(1).contains("mcu"))
            {
                data->setMcu(cmdList.at(2).toInt());
            }
        }
        else if(cmdList.at(0).contains("stm"))
        {
            if(cmdList.size() < 15)
            {
                qDebug() << "stm cmd size error";
                continue;
            }
            data->setWheel(cmdList.at(5).toFloat(), cmdList.at(6).toFloat(), cmdList.at(7).toFloat(), cmdList.at(8).toFloat());
            data->setLaser(cmdList.at(13).toFloat(), cmdList.at(14).toFloat(), cmdList.at(15).toFloat());
        }
        else if(cmdList.at(0).contains("log"))
        {
            if(cmdList.size() < 3)
            {
                qDebug() << "log cmd size error";
                continue;
            }
            QString originalLog = "";
            for(int i = 2; i < cmdList.size(); i++)
            {
                originalLog += cmdList.at(i);
                if(i < cmdList.size() - 1)
                    originalLog += ",";
            }
            if(cmdList.at(1).contains("d400"))
            {
                data->d400LogAdd(originalLog);
            }
            else if(cmdList.at(1).contains("t265"))
            {
                data->t265LogAdd(originalLog);
            }
            else if(cmdList.at(1).contains("rtabmap"))
            {
                data->rtabmapLogAdd(originalLog);
            }
            else if(cmdList.at(1).contains("lgdx_robot_auto_mode"))
            {
                data->pathPlannerLogAdd(originalLog);
            }
            else if(cmdList.at(1).contains("move_base"))
            {
                data->moveBaseLogAdd(originalLog);
            }
            else if(cmdList.at(1).contains("explore"))
            {
                data->exploreLogAdd(originalLog);
            }
        }
    }
}
