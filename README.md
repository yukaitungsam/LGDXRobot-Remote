# LGDXRobot-Remote

## About

A remote control application for LGDX Robot. It receives sensors data like image, positioning and distance. Also, it controls the robot by switiching operation modes and full control in manual mode.

## Screenshot

### Main Page

![Screenshot1](main.png)

### Auto Mode

![Screenshot2](auto.png)

## Dependency

Qt 5.15 

## Usage

Open .pro file using Qt Creator and execute the code. 
