﻿#ifndef SYSTEMDATA_H
#define SYSTEMDATA_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QCoreApplication>
#include <QDebug>
#include <QUrl>
#include <QDir>

class RobotStateClass
{
    Q_GADGET
public:
    enum Value {
        NotInit,
        Idle,
        AutoExplore,
        ManualExplore,
        AutoCollection,
        ManualAll,
        ManualP2P,
        Stopping,
    };
    Q_ENUM(Value)

private:
    explicit RobotStateClass();
};
using RobotState = RobotStateClass::Value;

class TCPConnectStateClass
{
    Q_GADGET
public:
    enum Value {
        Disconnected,
        Connecting,
        Connected,
        Error
    };
    Q_ENUM(Value)

private:
    explicit TCPConnectStateClass();
};
using TCPConnectState = TCPConnectStateClass::Value;

class SystemData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int tcpConnectState READ getTcpConnectStateInt NOTIFY TCPConnectStateChanged)
    Q_PROPERTY(QString tcpConnectError MEMBER tcpConnectError NOTIFY TCPConnectErrorChanged)
    Q_PROPERTY(int robotState READ getRobotStateInt NOTIFY robotStateChanged)
    Q_PROPERTY(QString robotIP MEMBER robotIP NOTIFY robotIPChanged)
    Q_PROPERTY(QString robotVideoUrl MEMBER robotVideoUrl NOTIFY robotIPChanged)
    Q_PROPERTY(QList<float> wheelSpeed MEMBER wheelSpeed NOTIFY wheelChanged)
    Q_PROPERTY(QList<int> laser MEMBER laser NOTIFY laserChanged)
    Q_PROPERTY(bool d400 MEMBER d400 NOTIFY d400Changed)
    Q_PROPERTY(bool t265 MEMBER t265 NOTIFY t265Changed)
    Q_PROPERTY(bool mcu MEMBER mcu NOTIFY mcuChanged)
    Q_PROPERTY(QStringList d400Log MEMBER d400Log NOTIFY d400LogChanged)
    Q_PROPERTY(QStringList t265Log MEMBER t265Log NOTIFY t265LogChanged)
    Q_PROPERTY(QStringList rtabmapLog MEMBER rtabmapLog NOTIFY rtabmapLogChanged)
    Q_PROPERTY(QStringList moveBaseLog MEMBER moveBaseLog NOTIFY moveBaseLogChanged)
    Q_PROPERTY(QStringList pathPlannerLog MEMBER pathPlannerLog NOTIFY pathPlannerLogChanged)
    Q_PROPERTY(QStringList exploreLog MEMBER exploreLog NOTIFY exploreLogChanged)
    Q_PROPERTY(QUrl ros2dPath MEMBER ros2dPath NOTIFY robotIPChanged)

private:
    static SystemData *instance;
    explicit SystemData(QObject *parent = nullptr);

    TCPConnectState tcpConnectState = TCPConnectState::Disconnected;
    QString tcpConnectError = "";
    RobotState robotState = RobotState::NotInit;
    QString robotIP = "";
    QString robotVideoUrl = "";
    QList<float> wheelSpeed = {0.0, 0.0, 0.0, 0.0};
    QList<int> laser = {0, 0, 0};
    bool d400 = false;
    bool t265 = false;
    bool mcu = false;
    QStringList d400Log;
    QStringList t265Log;
    QStringList rtabmapLog;
    QStringList moveBaseLog;
    QStringList pathPlannerLog;
    QStringList exploreLog;
    QUrl ros2dPath;

public:
    static SystemData *getInstance();

public slots:
    void setTcpConnectState(TCPConnectState s);
    TCPConnectState getTcpConnectState();
    int getTcpConnectStateInt();
    void setTcpConnectError(QString s);
    void setRobotState(RobotState s);
    void setRobotState(int s);
    void setRobotIP(QString ip);
    QString getRobotIP();
    RobotState getRobotState();
    int getRobotStateInt();
    void clearSensorData();

    // Log
    void d400LogAdd(QString str);
    void t265LogAdd(QString str);
    void rtabmapLogAdd(QString str);
    void moveBaseLogAdd(QString str);
    void pathPlannerLogAdd(QString str);
    void exploreLogAdd(QString str);
    void clearAllLog();

    // No get
    void setWheel(float v1, float v2, float v3, float v4);
    void setLaser(int v1, int v2, int v3);
    void setD400(bool b);
    void setT265(bool b);
    void setMcu(bool b);

signals:
    void TCPConnectStateChanged();
    void TCPConnectErrorChanged();
    void robotStateChanged();
    void robotIPChanged();
    void wheelChanged();
    void laserChanged();
    void d400Changed();
    void t265Changed();
    void mcuChanged();
    void d400LogChanged();
    void t265LogChanged();
    void moveBaseLogChanged();
    void rtabmapLogChanged();
    void pathPlannerLogChanged();
    void exploreLogChanged();
};

#endif // SYSTEMDATA_H
