#include "SystemData.h"
SystemData *SystemData::instance;

SystemData::SystemData(QObject *parent) : QObject(parent)
{

}

SystemData *SystemData::getInstance()
{
    if(instance == nullptr)
    {
        instance = new SystemData();
    }
    return instance;
}

void SystemData::setTcpConnectState(TCPConnectState s)
{
    tcpConnectState = s;
    emit TCPConnectStateChanged();
}

TCPConnectState SystemData::getTcpConnectState()
{
    return tcpConnectState;
}

int SystemData::getTcpConnectStateInt()
{
    return tcpConnectState;
}

void SystemData::setTcpConnectError(QString s)
{
    tcpConnectError = s;
    emit TCPConnectErrorChanged();
}

void SystemData::setRobotState(RobotState s)
{
    robotState = s;
    emit robotStateChanged();
}

void SystemData::setRobotState(int s)
{
    if(s == RobotState::NotInit)
        robotState = RobotState::NotInit;
    else if(s == RobotState::Idle)
        robotState = RobotState::Idle;
    else if(s == RobotState::AutoExplore)
        robotState = RobotState::AutoExplore;
    else if(s == RobotState::ManualExplore)
        robotState = RobotState::ManualExplore;
    else if(s == RobotState::AutoCollection)
        robotState = RobotState::AutoCollection;
    else if(s == RobotState::ManualAll)
        robotState = RobotState::ManualAll;
    else if(s == RobotState::ManualP2P)
        robotState = RobotState::ManualP2P;
    else if(s == RobotState::Stopping)
        robotState = RobotState::Stopping;
    emit robotStateChanged();
}

void SystemData::setRobotIP(QString ip)
{
    robotIP = ip;
    robotVideoUrl = "http://" + robotIP + ":8080/stream?topic=/d400/color/image_raw";
    QString ros2dPathStr = QDir::toNativeSeparators(QDir::currentPath() + "/ros3d.html");
    ros2dPath = QUrl().fromLocalFile(ros2dPathStr);
    emit robotIPChanged();
}

QString SystemData::getRobotIP()
{
    return robotIP;
}

RobotState SystemData::getRobotState()
{
    return robotState;
}

int SystemData::getRobotStateInt()
{
    return robotState;
}

void SystemData::clearSensorData()
{
    setWheel(0, 0, 0, 0);
    setLaser(0, 0, 0);
    setD400(false);
    setT265(false);
    setMcu(false);
}

void SystemData::d400LogAdd(QString str)
{
    d400Log.append(str);
    emit d400LogChanged();
}

void SystemData::t265LogAdd(QString str)
{
    t265Log.append(str);
    emit t265LogChanged();
}

void SystemData::rtabmapLogAdd(QString str)
{
    rtabmapLog.append(str);
    emit rtabmapLogChanged();
}

void SystemData::moveBaseLogAdd(QString str)
{
    moveBaseLog.append(str);
    emit moveBaseLogChanged();
}

void SystemData::pathPlannerLogAdd(QString str)
{
    pathPlannerLog.append(str);
    emit pathPlannerLogChanged();
}

void SystemData::exploreLogAdd(QString str)
{
    exploreLog.append(str);
    emit exploreLogChanged();
}

void SystemData::clearAllLog()
{
    d400Log.clear();
    emit d400LogChanged();
    t265Log.clear();
    emit t265LogChanged();
    rtabmapLog.clear();
    emit rtabmapLogChanged();
    moveBaseLog.clear();
    emit moveBaseLogChanged();
    pathPlannerLog.clear();
    emit pathPlannerLogChanged();
    exploreLog.clear();
    emit exploreLogChanged();
}

void SystemData::setWheel(float v1, float v2, float v3, float v4)
{
    wheelSpeed[0] = v1;
    wheelSpeed[1] = v2;
    wheelSpeed[2] = v3;
    wheelSpeed[3] = v4;
    emit wheelChanged();
}

void SystemData::setLaser(int v1, int v2, int v3)
{
    laser[0] = v1;
    laser[1] = v2;
    laser[2] = v3;
    emit laserChanged();
}

void SystemData::setD400(bool b)
{
    d400 = b;
    emit d400Changed();
}

void SystemData::setT265(bool b)
{
    t265 = b;
    emit t265Changed();
}

void SystemData::setMcu(bool b)
{
    mcu = b;
    emit mcuChanged();
}
