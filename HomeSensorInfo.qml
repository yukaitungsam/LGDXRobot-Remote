import QtQuick 2.0

Column {
    property string sensorName: "Sensor Name"
    property string sensorValue: "Value"
    property string imageLocation: ""

    Image {
        id: img
        height: 50
        width: 50
        source: imageLocation
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        text: sensorName + ": " + sensorValue
    }
}

