QT += quick network virtualkeyboard quickcontrols2 webview

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        System.cpp \
        SystemData.cpp \
        main.cpp

RESOURCES += qml.qrc \
    images.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    System.h \
    SystemData.h

DISTFILES += \
    js/eventemitter2.min.js \
    js/ros3d.min.js \
    js/roslib.min.js \
    js/three.min.js \
    ros3d.html

CONFIG += file_copies
COPIES += html
html.files = $$files($$PWD/*.html)
html.path = $$OUT_PWD
COPIES += js
js.files = $$files($$PWD/js/*.js)
js.path = $$OUT_PWD/js
